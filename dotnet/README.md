# Tpig.Components.Localization, .NET Version

A .NET WPF library to switch languages.  Here is sample:

    public MainWindow() {
        InitializeComponent();
        // initialize current locale to "en"
        LocaleManager.Instance.Current = "en";
        // load "en" language file/resource
        Localize.Load(this);
    }

After that, switching language is just:

    LocaleManager.Instance.Current = "th";

or:

    LocaleManager.Instance.Current = "en";


## Create String Files

First, you have to create string resource file.  This file should have to folder "Locales" (it can be change) and this file must have prefix "Lang" (still, it can be change).  Here is an example file "Locales\Lang.xaml":

    <ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                        xmlns:sys="clr-namespace:System;assembly=System.Runtime">
        <sys:String x:Key="Text1">Localization</sys:String>
    </ResourceDictionary>

When you want to use "Lang.xaml" in UI, first, include this in xaml, like this:

    <Window.Resources>
        <ResourceDictionary>
            <ResourceDictionary.MergedDictionaries>
                <ResourceDictionary Source="/Locales/Lang.xaml"/>
            </ResourceDictionary.MergedDictionaries>
        </ResourceDictionary>
    </Window.Resources>

and use DynamicResource, such as:

    <TextBlock Text="{DynamicResource Text1}"/>


## Create Localize Files

After you created "Lang.xaml".  You have to create localization file.  Example that you want to create Thai localization.  It can be done by create file "Lang.th-TH.xaml" or "Lang.th_TH.xaml" or just "Lang.th.xaml".

Code in "Lang.th.xaml" may look like this:

    <ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                        xmlns:sys="clr-namespace:System;assembly=System.Runtime">
        <sys:String x:Key="Text1">แปลเป็นภาษาไทย</sys:String>
    </ResourceDictionary>

Now, load file in construction UI, like this:

    public SomeWindow() {
        InitializeComponent();
        Localize.Load(this);
    }

Finally, when you want to change language, here:

    LocaleManager.Instance.Current = "th";


## Demo

These sample programs show how to use various features.

* Tpig.Demonstrates.Basic: provide enough basic functional
* Tpig.Demonstrates.TestNotify: show how to use interface NotifyChangeLanguage
* Tpig.Demonstrates.TestDll: show how to use DLL


## Requirements

* .NET version 6.0


## NuGet

You can retrieve package at https://www.nuget.org/packages/Tpig.Components.Localization.

