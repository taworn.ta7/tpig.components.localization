﻿using System;
using System.Windows;
using Tpig.Components.Localization;

namespace Tpig.Demonstrates.TestNotify;

/// <summary>
/// Interaction logic for AutoRefreshWindow.xaml
/// </summary>
public partial class AutoRefreshWindow : Window, INotifyChangeLanguage {

    public AutoRefreshWindow() {
        InitializeComponent();
        Localize.Load(this);
    }

    private void ChangeToEn(object sender, RoutedEventArgs e) {
        LocaleManager.Instance.Current = "en";
    }

    private void ChangeToTh(object sender, RoutedEventArgs e) {
        LocaleManager.Instance.Current = "th";
    }

    private void ChangeToJp(object sender, RoutedEventArgs e) {
        LocaleManager.Instance.Current = "jp";
    }

    public void BeforeChange() {
        Console.WriteLine("{0} shader visible", this);
        Shader.Visibility = Visibility.Visible;
    }

    public void AfterChange() {
        Shader.Visibility = Visibility.Hidden;
        Console.WriteLine("{0} shader hidden", this);
    }

}
