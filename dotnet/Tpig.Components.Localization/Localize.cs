﻿using System.Windows;

namespace Tpig.Components.Localization;

public static class Localize {

    /// <summary>
    /// Load text from resource, return resource Id if it not found.
    /// </summary>
    public static string Text(this FrameworkElement element, string resId) {
        var result = (string)element.TryFindResource(resId);
        if (result != null)
            return result;
        else
            return resId;
    }

    /// <summary>
    /// Convenient way to start locale loader and do other the rest.
    /// </summary>
    public static LocaleLoader Load(FrameworkElement owner) {
        var loader = new LocaleLoader(owner);
        owner.Loaded += (s, e) => LocaleManager.Instance.Add(owner, loader);
        owner.Unloaded += (s, e) => LocaleManager.Instance.Remove(owner, loader);
        return loader;
    }

    /// <summary>
    /// Convenient way to start locale loader.
    /// </summary>
    public static LocaleLoader LoadManual(FrameworkElement owner) {
        return new LocaleLoader(owner);
    }

}
