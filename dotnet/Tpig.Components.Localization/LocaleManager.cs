﻿using System.Collections.Generic;
using System.Threading;
using System.Windows;

namespace Tpig.Components.Localization;

/// <summary>
/// Localization manager.
/// </summary>
public class LocaleManager
{

    /// <summary>
    /// Singleton.
    /// </summary>
    public static LocaleManager Instance
    {
        get
        {
            if (instance == null)
                instance = new LocaleManager();
            return instance;
        }
    }
    private static LocaleManager? instance = null;

    /// <summary>
    /// Contructor.
    /// </summary>
    private LocaleManager()
    {
    }

    // ----------------------------------------------------------------------

    private class Item
    {
        public FrameworkElement? element = null;
        public LocaleLoader? loader = null;
    }

    private List<Item> list = new List<Item>();

    public void Add(FrameworkElement element, LocaleLoader loader)
    {
        list.Add(new Item
        {
            element = element,
            loader = loader
        });
    }

    public void Remove(FrameworkElement element, LocaleLoader loader)
    {
        var match = list.Find(i => i.element == element && i.loader == loader);
        if (match != null)
        {
            list.Remove(match);
        }
    }

    public void Update()
    {
        list.ForEach(i => (i.element as INotifyChangeLanguage)?.BeforeChange());
        var timer = (Timer?)null;
        timer = new Timer(
            (_0) =>
            {
                if (Application.Current?.Dispatcher != null)
                {
                    Application.Current?.Dispatcher.Invoke(() =>
                    {
                        list.ForEach(i => i.loader!.Locale = Current);
                        list.ForEach(i => (i.element as INotifyChangeLanguage)?.AfterChange());
                        timer?.Dispose();
                    });
                }
            }, null, 25, Timeout.Infinite);
    }

    // ----------------------------------------------------------------------

    /// <summary>
    /// Current locale property.
    /// </summary>
    public string Current
    {
        get => current;
        set
        {
            current = value;
            Update();
        }
    }
    private string current = "";

    /// <summary>
    /// Sub-directory, in case we search files in directory only.
    /// </summary>
    public string SubDir
    {
        get;
        set;
    } = "Locales";

    /// <summary>
    /// A string to prefix file/resource name.
    /// </summary>
    public string PrefixFileName
    {
        get;
        set;
    } = "Lang";

}
