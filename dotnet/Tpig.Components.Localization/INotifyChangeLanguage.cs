﻿namespace Tpig.Components.Localization;

/// <summary>
/// An interface to notify when caller change language.
/// </summary>
public interface INotifyChangeLanguage
{
    void BeforeChange();
    void AfterChange();
}
