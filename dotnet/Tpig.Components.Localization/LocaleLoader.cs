﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows;

namespace Tpig.Components.Localization;

/// <summary>
/// A file/resource locale loading.
/// </summary>
public class LocaleLoader
{

    public class LocaleInfo
    {
        public int ResourceId { get; set; } = -1;
        public string? File { get; set; } = null;
        public string? FileTemplate { get; set; } = null;
        public string? ResourceTemplate { get; set; } = null;
    }

    public List<LocaleInfo> List
    {
        get;
        private set;
    } = new List<LocaleInfo>();

    public string Locale
    {
        get => locale;
        set
        {
            if (locale != value)
            {
                Change(value);
                locale = value;
            }
        }
    }
    private string locale = "";

    public FrameworkElement? Owner
    {
        get;
        private set;
    } = null;

    // ----------------------------------------------------------------------

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="owner">The owner of locale loader.</param>
    /// <param name="locale">The string to assign locale, or null to use current default.</param>
    public LocaleLoader(FrameworkElement owner, string? locale = null)
    {
        Owner = owner;

        var dict = Owner.Resources.MergedDictionaries;
        for (var i = 0; i < dict.Count; i++)
            AddInList(dict[i]);

        if (!string.IsNullOrWhiteSpace(locale))
        {
            if (Locale != locale)
                Locale = locale;
        }
        else
        {
            if (Locale != LocaleManager.Instance.Current)
                Locale = LocaleManager.Instance.Current;
        }
    }

    // ----------------------------------------------------------------------

    private void Change(string? locale = null)
    {
        var culture = CultureToStrings(locale);
        var assemblyPath = Assembly.GetEntryAssembly()!.Location;
        var basePath = Path.Combine(Path.GetDirectoryName(assemblyPath)!, LocaleManager.Instance.SubDir);

        for (var i = 0; i < List.Count; i++)
        {
            var item = List[i];
            var uri = (Uri?)null;
            var resource = (ResourceDictionary?)null;

            // load from file
            var path = Path.Combine(basePath, string.Format(item.FileTemplate!, culture[0]));
            if (File.Exists(path))
                resource = LoadResource(new Uri(path));

            // no resource, but we have more locale string
            if (resource == null && culture.Length > 1)
            {
                path = Path.Combine(basePath, string.Format(item.FileTemplate!, culture[1]));
                if (File.Exists(path))
                    resource = LoadResource(new Uri(path));
            }

            // still null? find file in resources
            if (resource == null)
            {
                uri = new Uri(string.Format(item.ResourceTemplate!, culture[0]), UriKind.RelativeOrAbsolute);
                resource = LoadResource(uri);
            }

            // no resource again, but we have more locale string
            if (resource == null && culture.Length > 1)
            {
                uri = new Uri(string.Format(item.ResourceTemplate!, culture[1]), UriKind.RelativeOrAbsolute);
                resource = LoadResource(uri);
            }

            // put resource in proper place
            var dict = Owner?.Resources.MergedDictionaries!;
            if (resource != null)
            {
                if (item.ResourceId >= 0 && item.ResourceId < dict.Count)
                {
                    dict[item.ResourceId].Clear();
                    dict[item.ResourceId] = resource;
                }
                else
                {
                    dict.Add(resource);
                    item.ResourceId = dict.Count - 1;
                }
            }
            else
            {
                if (item.ResourceId >= 0 && item.ResourceId < dict.Count)
                {
                    dict[item.ResourceId].Clear();
                }
            }
        }
    }

    private void AddInList(ResourceDictionary item)
    {
        if (item.Source != null)
        {
            var prefix = LocaleManager.Instance.PrefixFileName;

            // check file with '.xaml' and prefix with 'Lang'
            var path = item.Source.ToString().Trim();
            var file = Path.GetFileName(path);
            if (file.StartsWith(prefix, StringComparison.OrdinalIgnoreCase))
            {
                if (file.EndsWith(".xaml", StringComparison.OrdinalIgnoreCase))
                {
                    var pathWithoutExt = path.Substring(0, path.Length - 5);
                    var resourceTemplate = string.Format("{0}.{1}.xaml", pathWithoutExt, "{0}");
                    var fileTemplate = string.Format("{0}.{1}.xaml", Path.GetFileNameWithoutExtension(file), "{0}");
                    List.Add(new LocaleInfo
                    {
                        File = path,
                        FileTemplate = fileTemplate,
                        ResourceTemplate = resourceTemplate
                    });
                }
            }
        }
    }

    private string[] CultureToStrings(string? locale = null)
    {
        var result = new List<string>();
        var culture = locale ?? Thread.CurrentThread.CurrentCulture.Name;

        // check if locale has '-' or '_' characters 
        var shortCulture = null as string;
        var delim = new char[] { '-', '_' };
        var parts = culture.Split(delim, 2);
        if (parts.Length == 2)
            shortCulture = parts[0];

        // return one or two elements
        result.Add(culture);
        if (shortCulture != null)
            result.Add(shortCulture);
        return result.ToArray();
    }

    private ResourceDictionary? LoadResource(Uri uri)
    {
        try
        {
            var result = new ResourceDictionary();
            result.Source = uri;
            return result;
        }
        catch (Exception)
        {
            Console.WriteLine("{0} cannot loaded!", uri);
            return null;
        }
    }

}
