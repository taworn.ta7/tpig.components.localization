﻿using System;
using System.Windows;
using Tpig.Components.Localization;

namespace Tpig.Demonstrates.TestDll;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window, INotifyChangeLanguage {

    public MainWindow() {
        InitializeComponent();
        LocaleManager.Instance.SubDir = "Resources/Languages";
        LocaleManager.Instance.PrefixFileName = "Language";
        LocaleManager.Instance.Current = "en";
        Localize.Load(this);
    }

    private void ChangeToEn(object sender, RoutedEventArgs e) {
        LocaleManager.Instance.Current = "en";
    }

    private void ChangeToTh(object sender, RoutedEventArgs e) {
        LocaleManager.Instance.Current = "th";
    }

    private void ChangeToJp(object sender, RoutedEventArgs e) {
        LocaleManager.Instance.Current = "jp";
    }

    private void AutoRefresh(object sender, RoutedEventArgs e) {
        Window window = new AutoRefreshWindow();
        window.Owner = this;
        window.Show();
    }

    private void ManualRefresh(object sender, RoutedEventArgs e) {
        Window window = new ManualRefreshWindow();
        window.Owner = this;
        window.ShowDialog();
    }

    private void DisplayMessage(object sender, RoutedEventArgs e) {
        var caption = this.Text("Caption");
        var message = this.Text("Message");
        MessageBox.Show(this, message, caption);
    }

    public void BeforeChange() {
        Console.WriteLine("{0} shader visible", this);
        Shader.Visibility = Visibility.Visible;
    }

    public void AfterChange() {
        Shader.Visibility = Visibility.Hidden;
        Console.WriteLine("{0} shader hidden", this);
    }

}
