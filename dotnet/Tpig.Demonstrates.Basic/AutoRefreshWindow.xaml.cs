﻿using System.Windows;
using Tpig.Components.Localization;

namespace Tpig.Demonstrates.Basic;

/// <summary>
/// Interaction logic for AutoRefreshWindow.xaml
/// </summary>
public partial class AutoRefreshWindow : Window {

    public AutoRefreshWindow() {
        InitializeComponent();
        Localize.Load(this);
    }

    private void ChangeToEn(object sender, RoutedEventArgs e) {
        LocaleManager.Instance.Current = "en";
    }

    private void ChangeToTh(object sender, RoutedEventArgs e) {
        LocaleManager.Instance.Current = "th";
    }

    private void ChangeToJp(object sender, RoutedEventArgs e) {
        LocaleManager.Instance.Current = "jp";
    }

}
