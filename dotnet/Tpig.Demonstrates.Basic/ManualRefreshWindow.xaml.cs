﻿using System.Windows;
using Tpig.Components.Localization;

namespace Tpig.Demonstrates.Basic;

/// <summary>
/// Interaction logic for ManualRefreshWindow.xaml
/// </summary>
public partial class ManualRefreshWindow : Window {

    private LocaleLoader? localeLoader = null;

    public ManualRefreshWindow() {
        InitializeComponent();
        localeLoader = Localize.LoadManual(this);
    }

    private void ChangeToEn(object sender, RoutedEventArgs e) {
        localeLoader!.Locale = "en";
    }

    private void ChangeToTh(object sender, RoutedEventArgs e) {
        localeLoader!.Locale = "th";
    }

    private void ChangeToJp(object sender, RoutedEventArgs e) {
        localeLoader!.Locale = "jp";
    }

}
